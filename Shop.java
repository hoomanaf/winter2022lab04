import java.util.Scanner;
public class Shop {
  public static void main(String[] args) {
    Scanner reader = new Scanner(System.in);
    
 laptop[] Laptop = new laptop[4];
    for (int i=0 ; i<Laptop.length ;i++) {
      
      System.out.println("Please enter your laptop's operating system");
      String os = reader.next();
      
      System.out.println("Please enter the colour of your laptop");
      String colour = reader.next();
      
      System.out.println("Please enter the screen size of your laptop");
      int screenSize = reader.nextInt();

      Laptop[i] = new laptop(os, screenSize, colour);
    }
    //Calling getters before updating
    System.out.println(Laptop[3].getOs());
    System.out.println(Laptop[3].getColour());
    System.out.println(Laptop[3].getScreenSize());
    
    //Setters outside the loop for updating the values
    System.out.println("Please enter a new value for your laptop's operating system");
    String newOs = reader.next();
    Laptop[3].setOs(newOs);
    
    System.out.println("Please enter a new value for the colour of your laptop");
    String newColour = reader.next();
    Laptop[3].setColour(newColour);
    
    //Calling getters after updating the values
    System.out.println(Laptop[3].getOs());
    System.out.println(Laptop[3].getColour());
    System.out.println(Laptop[3].getScreenSize());
    
    //Calling the instance method
    Laptop[3].feature();
  }
}
