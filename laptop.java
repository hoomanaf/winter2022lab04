public class laptop {
 private String os;
 private int screenSize;
 private String colour;

//Constructor
 public laptop(String os, int screenSize, String colour) {
  this.os = os;
  this.screenSize = screenSize;
  this.colour = colour;
 }
//Getters
 public String getOs() {
   return this.os; 
 }
 public int getScreenSize() {
   return this.screenSize;
 }
 public String getColour() {
  return this.colour; 
 }
 //Setters(Screen Size is deleted)
 public void setOs(String os) {
  this.os = os; 
 }
 public void setColour(String colour) {
   this.colour = colour; 
 }
 //Instance method
 public void feature() {
  if (this.os.equals("Mac")) {
  System.out.println("Cool my laptop's operating system is also MacOs!");
 }
 else {
  System.out.println("Okay, but I think MacOs is the best operating system :)");
 } 
if (this.screenSize>13) {
  System.out.println("It might be hard to fit your laptop in your backpack");
 }
 else {
  System.out.println("Nice, you should be comfortable working with your laptop");
 }
}
}